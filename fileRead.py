import pandas as pd
import time
from tkinter import Tk
from tkinter import filedialog

#System Variables
secToPause = input("Select sec of pause while reading your file")
rowPerTime = input("Select how may Row you want to read per time")

#Getting file to read
root = Tk()
root.withdraw()

print('Select csv file to read')
time.sleep(1)#1 sec pause before open file
file_path = filedialog.askopenfilename()

print('This program stops at Crtl+C or EOF')
print('Your file will start being read now:')
time.sleep(secToPause)

#Loop to read the file
while(True):
    try:
        data = pd.read_csv(file_path, skiprows=iteration-rowPerTime, nrows=rowPerTime)
		print (data)
		time.sleep(secToPause)

    except (KeyboardInterrupt, SystemExit):
        sys.exit()
    except (EOFError):
    	sys.exit()